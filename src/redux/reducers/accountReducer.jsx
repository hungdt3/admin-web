import { LOGIN, RECEIVE_LOGIN_RESPONSE, REGISTER, RECEIVE_REGISTER_RESPONSE } from "../../constants/action-types";

// Reducer to add article
const accountReducer = (state = [], action) => {
    let lastState = {};
    if (state.length > 0) {
        lastState = state[state.length - 1];
    }
    let newState = { ...lastState };

    switch (action.type) {
        case LOGIN:
            newState.login = action.payload;
            return [...state, newState];
        case RECEIVE_LOGIN_RESPONSE:
            newState.login_result = action.payload;
            return [...state, newState];
        case REGISTER:
            newState.register = action.payload;
            return [...state, newState];
        case RECEIVE_REGISTER_RESPONSE:
            newState.register_result = action.payload;
            return [...state, newState];
        default:
            return state;
    }
}

export default accountReducer;