import { combineReducers } from "redux";
import accountReducer from "./accountReducer";

// Combine all reducers as root reducer
export default combineReducers({ accounts: accountReducer });