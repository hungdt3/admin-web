import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "../reducers/index";
import thunk from 'redux-thunk';

const middleware = [thunk]
const enhancers = []
let composeEnhancers = compose

if (typeof window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ === 'function') {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
}

// Configure store with reducers and create
const store = createStore(rootReducer,
    composeEnhancers(
        applyMiddleware(...middleware),
        ...enhancers
    ));

export default store;
