import { LOGIN, RECEIVE_LOGIN_RESPONSE, REGISTER, RECEIVE_REGISTER_RESPONSE } from "../../constants/action-types";

// Action to add article to store
export const loginAction = data => ({
    type: LOGIN,
    payload: data
});

export const receiveLoginResponseAction = data => ({
    type: RECEIVE_LOGIN_RESPONSE,
    payload: data
});

export const registerAction = data => ({
    type: REGISTER,
    payload: data
});

export const receiveRegisterResponseAction = data => ({
    type: RECEIVE_REGISTER_RESPONSE,
    payload: data
});