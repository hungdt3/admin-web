import axios from 'axios';
import { baseApiUrl } from '../constants/apiUrls';
import { alertError } from '../services/alertService';

const ApiHelper = axios.create({
    baseURL: baseApiUrl
});

// ---- Use intercepter for requests ----
const requestHandler = (request) => {
    request.headers['Authorization'] = `Bearer ${sessionStorage.getItem('token')}`
    return request;
}
ApiHelper.interceptors.request.use(request => requestHandler(request));


// ---- Use intercepter for responses ----
const errorHandler = (error) => {
    console.log(error);
    return Promise.reject({ ...error })
}

const successHandler = (response) => {
    if (response) {
        if (response.status !== 200) {
            alertError(response.statusText);
        } else if (response.data.statusCode !== 0) {
            alertError(response.data.message);
        }
    }
    return response;
}

ApiHelper.interceptors.response.use(
    response => successHandler(response),
    error => errorHandler(error)
);

// ------------------------
export default ApiHelper;
