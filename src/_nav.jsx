export default {
    items: [{
            title: true,
            name: 'Navigation',
            wrapper: {
                element: '',
                attributes: {},
            },
        },
        {
            name: 'Admin',
            url: '/admin',
            icon: 'icon-wrench',
            children: [{
                name: 'User',
                url: '/admin/users',
                icon: 'icon-user'
            }]
        },
        
    ],
};