import Swal from 'sweetalert2';

export function alertSuccess(message, callBack) {
    Swal.fire({
        title: 'Success',
        type: 'success',
        text: message,
        onAfterClose: callBack
    });
}

export function alertError(message, callBack) {
    Swal.fire({
        title: "Oops..",
        type: "error",
        text: message,
        onAfterClose: callBack
    });
}