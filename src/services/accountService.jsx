import { loginAction, receiveLoginResponseAction, registerAction, receiveRegisterResponseAction } from '../redux/actions/accountActions';
import { apiUrls } from '../constants/apiUrls';
import ApiHelper from '../helpers/apiHelper';

export function login(email, password, dispatch) {
    var payload = {
        "email": email,
        "password": password
    }

    dispatch(loginAction(payload));
    return ApiHelper.post(apiUrls.apiLogin, payload)
        .then(response => {
            dispatch(receiveLoginResponseAction(response));
            return response;
        });
}

export function register(payload, dispatch) {
    dispatch(registerAction(payload));
    return ApiHelper.post(apiUrls.apiRegister, payload)
        .then(response => {
            dispatch(receiveRegisterResponseAction(response));
            return response;
        });
}

export function getProfile(dispatch) {
    return ApiHelper.get(apiUrls.apiAccount)
        .then(response => {
            return response;
        });
}

export function updateProfile(payload, dispatch) {
    return ApiHelper.put(apiUrls.apiAccount, payload)
        .then(response => {
            return response;
        });
}
