import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { login } from '../../services/accountService';
import { AvForm, AvFeedback, AvGroup, AvInput } from 'availity-reactstrap-validation';
import { alertError } from '../../services/alertService';

const mapDispatchToProps = (dispatch) => {
  return {
    login: (email, password) => login(email, password, dispatch)
  };
};

class ConnectedLogin extends Component {

  constructor(props) {
    super(props);
    this.handleValidSubmit = this.handleValidSubmit.bind(this);
  }

  handleValidSubmit(event, values) {
    const self = this;
    this.props.login(values.email, values.password)
      .then(function (response) {
        debugger;
        if (response && response.data && response.data.statusCode === 0) {
          sessionStorage.setItem('token', response.data.data.access_token);
          self.props.history.push('/');
        }
      });
  }

  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <AvForm onValidSubmit={this.handleValidSubmit}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <AvGroup>
                        <InputGroup className="mb-3">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-user"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <AvInput name="email" type="text" placeholder="Email" autoComplete="email" required />
                          <AvFeedback>This field can not be empty</AvFeedback>
                        </InputGroup>
                      </AvGroup>

                      <AvGroup>
                        <InputGroup className="mb-4">
                          <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                              <i className="icon-lock"></i>
                            </InputGroupText>
                          </InputGroupAddon>
                          <AvInput name="password" type="password" placeholder="Password" autoComplete="current-password" required />
                          <AvFeedback>This field can not be empty</AvFeedback>
                        </InputGroup>
                      </AvGroup>
                      <Row>
                        <Col xs="6">
                          <Button type="submit" color="primary" className="px-4">Login</Button>
                        </Col>
                      </Row>
                    </AvForm>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Please create an account to access this website.</p>
                      <Link to="/register">
                        <Button color="primary" className="mt-3" active tabIndex={-1}>Register Now!</Button>
                      </Link>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

const Login = connect(null, mapDispatchToProps)(ConnectedLogin);
ConnectedLogin.propTypes = {
  login: PropTypes.func.isRequired,
};

export default Login;
