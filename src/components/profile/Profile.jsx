import React, { Component } from 'react';
import { Col, Card, CardHeader, CardBody, Row, Button } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { getProfile, updateProfile } from '../../services/accountService';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { alertError, alertSuccess } from '../../services/alertService';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import './Profile.scss';

const mapDispatchToProps = (dispatch) => {
    return {
        getProfile: () => getProfile(dispatch),
        updateProfile: (payload) => updateProfile(payload, dispatch)
    };
};

export class ConnectedProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isEdit: false,
            profile: {},
            dob: new Date()
        };
        this.handleValidSubmit = this.handleValidSubmit.bind(this);
    }

    componentDidMount() {
        this.props.getProfile().then(response => {
            if (response && response.data) {
                if (response.data.statusCode === 0) {
                    this.setState({ profile: response.data.data });
                    this.setState({ dob: new Date(response.data.data.doB) })
                } else {
                    alertError(response.data.message);
                }
            }
        });
    }

    handleValidSubmit(event, values) {
        debugger;
        const payload = {
            name: values.name,
            mobileNumber: values.mobileNumber,
            gender: values.gender,
            dob: this.state.dob,
            emailOptIn: values.emailOptIn
        };
        this.props.updateProfile(payload).then(response => {
            if (response && response.data) {
                if (response.data.statusCode === 0) {
                    alertSuccess('Update profile successfully', () => this.setState({ isEdit: false, profile: response.data.data }));
                } else {
                    alertError(response.data.message);
                }
            }
        });
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col lg="6" md="6" sm="6" xs="12" >
                        <Card>
                            <CardBody>
                                <img src={'../../assets/img/declan.jpg'} style={{ maxWidth: "100%" }}></img>
                            </CardBody>
                        </Card>
                    </Col>

                    <Col lg="6" md="6" sm="6" xs="12">
                        <AvForm onValidSubmit={this.handleValidSubmit}>
                            <Card>
                                <CardHeader>
                                    <strong>Profile</strong>
                                    <div style={{ float: "right" }}>
                                        <Button color="primary" style={{ marginRight: 5 }} disabled={this.state.isEdit} onClick={() => this.setState({ isEdit: true })}><i className="icon-pencil"></i> Edit</Button>
                                        <Button type="submit" disabled={!this.state.isEdit} color="success" ><i className="icon-check"></i> Save</Button>
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <AvField name="name" label="Name" type="text"
                                        value={this.state.profile.name}
                                        disabled={this.state.isEdit === false}
                                        validate={{
                                            required: { value: true, errorMessage: 'This field can not be empty.' },
                                            maxLength: { value: 256 }
                                        }} />
                                    <AvField name="email" label="Email" type="text"
                                        value={this.state.profile.email}
                                        disabled={true}
                                    />
                                    <AvField name="mobileNumber" label="Mobile Number" type="text"
                                        value={this.state.profile.mobileNumber}
                                        disabled={this.state.isEdit === false}
                                        validate={{
                                            required: { value: true, errorMessage: 'This field can not be empty.' },
                                            maxLength: { value: 32 }
                                        }} />

                                    <AvField type="select" label="Gender" name="gender"
                                        value={this.state.profile.gender}
                                        disabled={this.state.isEdit === false}>
                                        <option value="1">Male</option>
                                        <option value="0">Female</option>
                                    </AvField>

                                    <div className={this.state.dob ? "form-group" : "form-group text-danger"}>
                                        <label>Date Of Birth</label>
                                        <DatePicker name="dob"
                                            className={this.state.dob ? "form-control" : "form-control is-invalid"}
                                            selected={this.state.dob}
                                            onChange={date => this.setState({ dob: date })}
                                            disabled={this.state.isEdit === false} />
                                        <div className="invalid-feedback" style={this.state.dob ? null : { "display": "block" }}>This field can not be empty.</div>
                                    </div>

                                    <AvField name="emailOptIn" label="Email OptIn" type="emailOptIn"
                                        value={this.state.profile.emailOptIn}
                                        disabled={this.state.isEdit === false}
                                        validate={{
                                            required: { value: true, errorMessage: 'This field can not be empty.' },
                                            email: { value: true, errorMessage: 'This field must follows the email pattern.' },
                                            maxLength: { value: 256 }
                                        }} />
                                </CardBody>
                            </Card>
                        </AvForm>
                    </Col>
                </Row>

            </div>
        );
    }
}

const Profile = connect(null, mapDispatchToProps)(ConnectedProfile);
ConnectedProfile.propTypes = {
    getProfile: PropTypes.func.isRequired,
    updateProfile: PropTypes.func.isRequired,
};

export default Profile;
