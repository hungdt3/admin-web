import React, { Component } from 'react';
import { Button, Card, CardBody, Col, Container, Row } from 'reactstrap';
import { register } from '../../services/accountService';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import { alertSuccess, alertError } from '../../services/alertService';

const mapDispatchToProps = (dispatch) => {
    return {
        register: (payload) => register(payload, dispatch)
    };
};


class ConnectedRegister extends Component {

    constructor(props) {
        super(props);
        this.handleValidSubmit = this.handleValidSubmit.bind(this);
    }

    handleValidSubmit(event, values) {
        const self = this;
        const payload = {
            name: values.name,
            email: values.email,
            password: values.password,
            mobileNumber: values.mobileNumber,
            gender: values.gender,
            dob: values.dob,
            emailOptIn: values.emailOptIn
        };
        this.props.register(payload).then(response => {
            if (response && response.data) {
                if (response.data.statusCode === 0) {
                    alertSuccess('You have register successfully. We will redirect to login page now.', () => {
                        self.props.history.push('/');
                    });
                } else {
                    alertError(response.data.message);
                }
            }
        });
    }

    render() {
        const defaultValues = {
            gender: 1
        };
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="9" lg="7" xl="6">
                            <Card className="mx-4">
                                <CardBody className="p-4">
                                    <AvForm onValidSubmit={this.handleValidSubmit} model={defaultValues}>
                                        <h1>Register</h1>
                                        <p className="text-muted">Create your account</p>

                                        <AvField name="name" placeholder="Name" type="text"
                                            validate={{
                                                required: { value: true, errorMessage: 'This field can not be empty.' },
                                                maxLength: { value: 256 }
                                            }} />
                                        <AvField name="email" placeholder="Email" type="email"
                                            validate={{
                                                required: { value: true, errorMessage: 'This field can not be empty.' },
                                                email: { value: true, errorMessage: 'This field must follows the email pattern.' },
                                                maxLength: { value: 256 }
                                            }} />
                                        <AvField name="password" placeholder="Password" type="password"
                                            validate={{
                                                required: { value: true, errorMessage: 'This field can not be empty.' },
                                                maxLength: { value: 128 }
                                            }} />
                                        <AvField name="repeatPassword" placeholder="Repeat Password" type="password"
                                            validate={{
                                                required: { value: true, errorMessage: 'This field can not be empty.' },
                                                maxLength: { value: 256 },
                                                match: { value: 'password', errorMessage: 'This field must be same as the field of Password.' }
                                            }} />
                                        <AvField name="mobileNumber" placeholder="Mobile Number" type="text"
                                            validate={{
                                                required: { value: true, errorMessage: 'This field can not be empty.' },
                                                maxLength: { value: 32 }
                                            }} />

                                        <AvField type="select" name="gender" helpMessage="Select gender to continue">
                                            <option value="1">Male</option>
                                            <option value="0">Female</option>
                                        </AvField>

                                        <AvField type="date" name="dob" helpMessage="Select the date of birth"
                                            validate={{
                                                required: { value: true, errorMessage: 'This field can not be empty.' }
                                            }} />
                                        <AvField name="emailOptIn" placeholder="Email OptIn" type="emailOptIn"
                                            validate={{
                                                required: { value: true, errorMessage: 'This field can not be empty.' },
                                                email: { value: true, errorMessage: 'This field must follows the email pattern.' },
                                                maxLength: { value: 256 }
                                            }} />

                                        <Button type="submit" color="success" block>Create Account</Button>
                                    </AvForm>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}


const Register = connect(null, mapDispatchToProps)(ConnectedRegister);
ConnectedRegister.propTypes = {
    register: PropTypes.func.isRequired,
};
export default Register;
