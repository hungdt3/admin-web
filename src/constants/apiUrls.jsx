export const baseApiUrl = 'https://admin-core.azurewebsites.net';

export const apiUrls = {
    apiAccount: '/api/accounts',
    apiLogin: '/api/accounts/login',
    apiRegister: '/api/accounts/register'
}