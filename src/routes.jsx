import UserList from './components/user/UserList';
import Profile from './components/profile/Profile';

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/profile', name: 'Profile', component: Profile },
  { path: '/admin', exact: true, name: 'Admin', component: UserList },
  { path: '/admin/users', name: 'Users', component: UserList },
];

export default routes;
